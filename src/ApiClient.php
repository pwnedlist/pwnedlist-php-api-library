<?php 

namespace PwnedList;
 
class ApiClient {
 
 	public $key;
	public $secret;
 	public $endPoint;
	public $serverResponse;
 
 	public function makeCall($method, $payload, $httpMethod="GET") {
 		$payload = $this->buildPayload($payload, $method, $this->key, $this->secret);
		$payloadString = http_build_query($payload);
		$path = str_replace('.', '/', $method);
 		$handler = curl_init();

		if (strtoupper($httpMethod) == "POST") {
			//POST Case
			$url = $this->endPoint . $path;
			curl_setopt($handler, CURLOPT_POST, true);
			curl_setopt($handler, CURLOPT_POSTFIELDS, $payloadString);
		} else {
			//GET Case
			$url = $this->endPoint . $path . '?' . $payloadString;
		}
		curl_setopt($handler, CURLOPT_URL, $url); 
		curl_setopt($handler, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec($handler);
		$json = json_decode($result);

		if ($json === NULL) {
			$this->serverResponse = $result;
			return false;
		} else {
			return $json;
		}

		curl_close($handler);
		
		return $result;
		
 	}

 	private function buildPayload($oldPayload, $method, $key, $secret) {
 		$oldPayload['ts'] = time();
	 	$oldPayload['key'] = $key;
	 	$oldPayload['hmac'] = $this->genHmac($method, $oldPayload['ts'] , $key, $secret);
		return $oldPayload;
	}

	private function genHmac($method, $ts, $key, $secret) {
		$msg = $key . $ts . $method . $secret;
		$hm = hash_hmac('sha1', $msg, $secret, false);
		return $hm;
	}

	public function __construct($key, $secret, $endPoint="https://api.pwnedlist.com/api/1/") {
		$this->key = $key;
		$this->secret = $secret;
		$this->endPoint = $endPoint;		
	}
 
}