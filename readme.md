#Pwnedlist API PHP Client Library

This client library provides a PHP interface to the PwnedList API.  You will need valid PwnedList credentials.

This library can be installed via Composer.  Simply add an entry for "require" and a custom repository.

    {
        "require": {
                "pwnedlist/pwnedlist-php-api-library" : "dev-master"
        },

        "repositories": [
                {
                        "type": "vcs",
                        "url": "http://bitbucket.org/pwnedlist/pwnedlist-php-api-library.git"
                }
        ]
    }
    
Composer's autoloader will generate the correct path to the source files.  

To use the client, pass your key and secret at instantiation.  Use makeCall to make the API call.  This call takes three parameters.  The first is a string of the PwnedList method name. The second parameter is the payload for the call stored as an associative array.  The final parameter is the HTTP Method.  This method is optional and defaults to "GET."

    require "vendor/autoload.php";
    use PwnedList\ApiClient;
    $c = new ApiClient("YOUR KEY", "YOUR SECRET");
    $results = $c->makeCall('PWNEDLIST API METHOD NAME', PARAMETERS AS ARRAY, "HTTP METHOD");

	//Example:
	//$results = $c->makeCall('domains.query', ["domain_identifier" => "gmail.com"], "GET");

If the call was successful, results will be returned as a standard object which you can traverse.

    echo $results->{'count'} . "\n";
	echo $results->{'token'} . "\n";
	echo $results->{'domain_identifier'};
  
If an error occured, makeCall will return false and the error will be stored in the serverResponse property.

    if ($results === false) {
        var_dump($c->serverResponse);
    }